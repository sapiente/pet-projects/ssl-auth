
# CA certificate

openssl req -newkey rsa:4096 -keyform PEM -keyout ca.key -x509 -days 3650 -outform PEM -out ca.crt

openssl x509 -in ca.crt -noout -text

# server certificate

openssl genrsa -out server.key 4096
openssl req -new -key server.key -out server.req -sha256
openssl x509 -req -in server.req -CA ca.crt -CAkey ca.key -set_serial 100 -extensions server -days 1460 -outform PEM -out server.crt -sha256

openssl x509 -in server.crt -noout -text

# client certificate

openssl genrsa -out client.key 4096
openssl req -new -key client.key -out client.req
openssl x509 -req -in client.req -CA ca.crt -CAkey ca.key -set_serial 101 -extensions client -days 365 -outform PEM -out client.crt

openssl x509 -in client.crt -noout -text


# Create pkcs from client cert

openssl pkcs12 -export -in ca.crt -inkey ca.key -out ca.p12 -name ca -CAfile ca.crt -caname root
openssl pkcs12 -info -in ca.p12 -passin pass:test

# JKS 

keytool -genkeypair -alias boguscert -storepass changeit -keypass secretPassword -keystore test.jks -dname "CN=Developer, OU=Department, O=Company, L=City, ST=State, C=CA"
keytool -delete -alias boguscert -storepass changeit -keystore test.jks

keytool -importkeystore -deststorepass changeit -destkeypass changeit -destkeystore test.jks -srckeystore ./docker/ca.p12 -srcstoretype PKCS12 -srcstorepass test -alias ca


keytool -list -keystore test.jks -storepass changeit

# download cert

true | openssl s_client -connect google.com:443 2>/dev/null | openssl x509

# verify crt & key

openssl x509 -noout -modulus -in ca.crt | openssl md5

1565cc4a2d7eb4ae732fab8c8749719f

openssl rsa -noout -modulus -in ca.key | openssl md5

Enter pass phrase for ca.key:

1565cc4a2d7eb4ae732fab8c8749719f

# verify client & ca certificates

openssl verify -verbose -CAfile cacert.pem  server.crt
