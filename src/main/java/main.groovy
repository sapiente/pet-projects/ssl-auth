import groovy.transform.Field
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient
import org.apache.http.conn.scheme.Scheme
import org.apache.http.conn.ssl.SSLSocketFactory
import org.apache.http.conn.ssl.TrustSelfSignedStrategy
import org.apache.http.conn.ssl.X509HostnameVerifier
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.ssl.TrustStrategy

import javax.net.ssl.SSLContext
import java.security.KeyManagementException
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import java.security.UnrecoverableKeyException

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.TEXT

@Field String CERTIFICATE_KEY_FILE_PATH = "./client.jks"
@Field String CERTIFICATE_PASSWORD = "changeit"
@Field String CERTIFICATE_KEY_TYPE = "jks"

//println(getDataWithHttp())
println(getDataWithHttps())

def getDataWithHttp() {
    final String url = "http://localhost:8081"
    final RESTClient restClient = new RESTClient(url)

    final HttpResponseDecorator result = restClient.get([
            contentType: JSON,
            path       : "/"
    ]) as HttpResponseDecorator

    return result.getData()
}

def getDataWithHttps() {
    final String url = "https://localhost:1800"
    final RESTClient restClient = getSslRestClient(url)

    final HttpResponseDecorator result = restClient.get([path       : "/v1/terminals", contentType: TEXT]) as HttpResponseDecorator

    return result.getData()
}

RESTClient getSslRestClient(final String url) {
    final KeyStore keyStore = getKeyStore()
    SSLContext sslContext
    try {
        sslContext = SSLContextBuilder.create()
                .loadKeyMaterial(keyStore, CERTIFICATE_PASSWORD.toCharArray())
                .loadTrustMaterial(keyStore, new TrustSelfSignedStrategy() as TrustStrategy).build()
    } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException | UnrecoverableKeyException e) {
        throw new IllegalStateException("Could not create SSL context for communication with TMS.", e)
    }

    final RESTClient restClient = new RESTClient(url)
    final Scheme scheme = new Scheme("https", 443,
            new SSLSocketFactory(sslContext, { hostname, sslSession -> true } as X509HostnameVerifier))
    restClient.client.connectionManager.schemeRegistry.register(scheme)

    return restClient
}

KeyStore getKeyStore() {
    final KeyStore keyStore = KeyStore.getInstance(CERTIFICATE_KEY_TYPE)

    final File file = new File(CERTIFICATE_KEY_FILE_PATH)
    final InputStream inputStream = new FileInputStream(file)
    keyStore.load(inputStream, CERTIFICATE_PASSWORD.toCharArray())

    return keyStore
}
